'''
Created on Dec 4, 2010

@author: nader
'''

config_dict = {"version":1,
               "disable_existing_loggers": False,
               "formatters":{
                             "simple":{
                                       "format":"%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                                       "datefmt":""}},
               "handlers":{
                           "console":{
                                      "class":"logging.StreamHandler",
                                      "level":"DEBUG",
                                      "formatter":"simple",
                                      "stream":"ext://sys.stdout"}
                           },
               "loggers":{
                          "shareislam":{
                                                "level":"DEBUG",
                                                "handlers":["console"],
                                                "qualname":"shareislam",
                                                "propagate":False},
                          "requests":{
                                                "level":"WARN",
                                                "handlers":["console"],
                                                "qualname":"shareislam",
                                                "propagate":False},
                          },
                            
              "root":{
                      "level":"DEBUG",
                      "handlers":["console"]},
               }

def configure_logger(loggerName):
    import logging.config
    logging.config.dictConfig(config_dict)
    return logging.getLogger(loggerName)
