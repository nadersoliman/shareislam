# -*- coding: utf-8 -*-
'''
Created on Jun 30, 2013

@author: nader
'''
import sys, re
from StringIO import StringIO
import json

from shareislam.grabber import Grabber
from shareislam.scraper import Scraper
    
if __name__ == '__main__':
    #Grabber.grab_all()
    Scraper.scrape_all()
