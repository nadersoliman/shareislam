# -*- coding: utf-8 -*-
'''
Created on Sep 5, 2013

@author: nader
'''
import os

import lxml.html
import lxml.etree
from lxml.html import parse, builder as H

from shareislam import map_index_to_orginial_html, generate_yaml_docs
import log_configs

logger = log_configs.configure_logger('shareislam2.grabber')

class Scraper(object):
    '''
    classdocs
    '''

    def __init__(self, ydoc):
        '''
        @param ydoc: the yaml doc you got from the grabber 
        '''
        self.ydoc = ydoc
        self.force_scrape = False
        self.html_dom = None
        
    def scrape(self):
        fname = map_index_to_orginial_html(self.ydoc['index'], self.ydoc['url'])
        if os.path.exists(fname) and not self.force_scrape:
            logger.info('skipping %03d %s as already exists' % (self.ydoc['index'], self.ydoc['name']['tr']))
        else:
            logger.info('scraping %03d %s' % (self.ydoc['index'], self.ydoc['name']['tr']))
            with open(fname, 'w+') as f:
                self.html_dom = parse(f)
                
        self.find_translation_table()
                
    def find_translation_table(self):
        pass

    @staticmethod
    def scrape_all():
        for ydoc in generate_yaml_docs():
            scraper = Scraper(ydoc)
            scraper.scrape()
