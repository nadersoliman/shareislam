import os

import yaml

def map_index_to_orginial_html(index, url):
    fname = None
    if index == 2:
        if url.lower().endswith('ii.htm'):
            fname = '%03d-p2.html' % (index)
        else:
            fname = '%03d-p1.html' % (index)
    return map_index_to(index, url, 'original', 'html', fname) 

def map_index_to_output_html(index, url):
    return map_index_to(index, url, 'output', 'html') 

def map_index_to_output_yaml(index, url):
    return map_index_to(index, url, 'output', 'yaml') 
    fname = '%03d.html' % (index)
    return os.path.join(os.path.dirname(__file__), '..', 'output', fname)

def map_index_to(index, url, directory, ext, fname=None):
    fname = fname if fname else '%03d.%s' % (index, ext)
    return os.path.join(os.path.dirname(__file__), '..', directory, fname)

def generate_yaml_docs():
    with open(os.path.join(os.path.dirname(__file__), '..', 'original', 'index.yaml'), 'r') as f:
        for ydoc in yaml.load_all(f): 
            yield ydoc 


HEADERS = {
           'User-Agent': 'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; Media Center PC 4.0; SLCC1; .NET CLR 3.0.04320)',
           'Accept': 'text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5',
           'Accept-Language': 'ar-eg,en;q=0.7,en-us;q=0.3',
           'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.7',
           'Connection':'keep-alive'}
    