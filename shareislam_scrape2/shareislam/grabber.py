# -*- coding: utf-8 -*-
'''
Created on Sep 5, 2013

@author: nader
'''
import os

import yaml
import requests

import log_configs
from shareislam import map_index_to_orginial_html, generate_yaml_docs, HEADERS

logger = log_configs.configure_logger('shareislam2.grabber')

class Grabber(object):
    '''
    classdocs
    '''
    
    def __init__(self, ydoc):
        '''
        @param ydoc: the yaml doc you got from the grabber 
        '''
        self.ydoc = ydoc
        self.force_download = False
        
    def grab(self):
        fname = map_index_to_orginial_html(self.ydoc['index'], self.ydoc['url'])
        if os.path.exists(fname) and not self.force_download:
            logger.info('skipping %03d %s as already exists' % (self.ydoc['index'], self.ydoc['name']['tr']))
        else:
            logger.info('grabbing %03d %s' % (self.ydoc['index'], self.ydoc['name']['tr']))
            response = requests.get(self.ydoc['url'], headers=HEADERS)
            with open(fname, 'w+') as f:
                f.write(response.text)
                
    @staticmethod
    def grab_all():
        for ydoc in generate_yaml_docs():
            grabber = Grabber(ydoc)
            grabber.grab()
