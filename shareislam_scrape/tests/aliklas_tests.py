# -*- coding: utf-8 -*-
'''
Created on Jul 10, 2013

@author: nader
'''
from mock import patch, Mock

import unittest
import pprint
from shareislam.scraper import Scraper

pp = pprint.PrettyPrinter(indent=4)
class Test(unittest.TestCase):


    def setUp(self):
        self.page_html = None
        with open('tests/alikhlas_test.htm', 'r') as f:
            self.page_html = f.read()
        
    @patch('shareislam.scraper.sys')
    def test_scrape_page(self, scraper_sys):
        scraper = Scraper()
        scraper.scrape_page(self.page_html)
        self.assertEqual(0, scraper_sys.exit.call_count)
        self.assertEqual('Surah Akhlas', scraper._surah_name)
        
    @patch('shareislam.scraper.sys')
    def test_collect_parts(self, scraper_sys):
        scraper = Scraper()
        def pass_build_parts():
            pass
        scraper.build_parts_trees = pass_build_parts
        scraper.scrape_page(self.page_html)
        self.assertEqual(0, scraper_sys.exit.call_count)
        self.assertEqual(180, len(scraper._parts))
        self.assertTrue('SECTION' in scraper._parts[0]['ptype'])
        self.assertEqual(set(['ARABIC', 'AYAH', 'SECTION']), scraper._parts[103]['ptype'])
        
#         for idx, part in enumerate(scraper._parts):
#             print 'part idx:%s', idx
#             pp.pprint(part) 
        
    @patch('shareislam.scraper.sys')
    def test_build_parts(self, scraper_sys):
        scraper = Scraper()
        scraper.scrape_page(self.page_html)
        self.assertEqual(0, scraper_sys.exit.call_count)
        scraper.build_parts_trees()
        
#         for idx, part in enumerate(scraper._parts_trees):
#             print 'tree part idx:%s', idx
#             pp.pprint(part) 
        
    
    @patch('shareislam.scraper.sys')
    def test_detect_part_type(self, scraper_sys):
        scraper = Scraper()
        scraper.scrape_page(self.page_html)
        self.assertEqual(0, scraper_sys.exit.call_count)
        
        parts = scraper._tree.xpath('//p')
        
        for idx, p in enumerate(parts):
            print 'parts idx:%s, type:%s' % (idx, scraper._detect_part_type(p))
        
        s, _ = scraper._detect_part_type(parts[153])
        self.assertEqual(set(['SECTION']), s)
        
        s, _ = scraper._detect_part_type(parts[171])
        self.assertEqual(set(['BULLET']), s)
        
        s, _ = scraper._detect_part_type(parts[202])
        self.assertEqual(set(['AYAH', 'ARABIC', 'SECTION']), s)
        
        s, _ = scraper._detect_part_type(parts[84])
        self.assertEqual(set([]), s)
        
        s, _ = scraper._detect_part_type(parts[210])
        self.assertEqual(set(['XAYAH', 'ARABIC']), s)
        
        s, _ = scraper._detect_part_type(parts[106])
        self.assertEqual(set(['ARABIC', 'HADITH']), s)
        
    @patch('shareislam.scraper.sys')
    def test_write_output(self, scraper_sys):
        scraper = Scraper()
        scraper.scrape_page(self.page_html)
        self.assertEqual(0, scraper_sys.exit.call_count)
        with open('112.html', 'w') as html_stream:
            scraper.write_html(html_stream)
        with open('112.json', 'w') as json_stream:
            scraper.write_json(json_stream)
        with open('112.json', 'r') as json_stream:
            scraper.read_json(json_stream)
            self.assertTrue(len(scraper._parts_trees) > 0)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()