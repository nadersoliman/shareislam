# -*- coding: utf-8 -*-
'''
Created on Jul 10, 2013

@author: nader
'''
from mock import patch, Mock

import unittest
import pprint
from shareislam.scraper import Scraper

pp = pprint.PrettyPrinter(indent=4)
class Test(unittest.TestCase):


    def setUp(self):
        self.page_html = None
        with open('tests/quraish_test.htm', 'r') as f:
            self.page_html = f.read()
        
    @patch('shareislam.scraper.sys')
    def test_collect_parts(self, scraper_sys):
        scraper = Scraper()
        scraper.scrape_page(self.page_html)
        self.assertEqual(0, scraper_sys.exit.call_count)
        self.assertEqual(0, len(scraper._parts))
        self.assertEqual(8, len(scraper._parts_trees))
        
        for idx, part in enumerate(scraper._parts_trees):
            print 'part idx:%s', idx
            pp.pprint(part)
            
        ayat = self.collect_ayah(scraper._parts_trees)
        self.assertEqual(4, len(ayat))
        for idx, ayah in enumerate(ayat):
            self.assertEqual(idx+1, ayah['ayah_idx'])
            
    def collect_ayah(self, parts_trees):
        ayat = []
        for part in parts_trees:
            if 'AYAH' in part['ptype']:
                ayat.append(part)
            for sub in part['sub']:
                ayat.extend(self.collect_ayah([sub]))
        return ayat
            
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()