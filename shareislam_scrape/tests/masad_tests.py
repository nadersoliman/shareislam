# -*- coding: utf-8 -*-
'''
Created on Jul 10, 2013

@author: nader
'''
from mock import patch, Mock

import unittest
import pprint
from shareislam.scraper import Scraper

pp = pprint.PrettyPrinter(indent=4)
class Test(unittest.TestCase):


    def setUp(self):
        self.page_html = None
        with open('tests/masad_test.htm', 'r') as f:
            self.page_html = f.read()
        
    @patch('shareislam.scraper.sys')
    def test_scrape_page(self, scraper_sys):
        scraper = Scraper()
        scraper.scrape_page(self.page_html)
        self.assertEqual(0, scraper_sys.exit.call_count)
        self.assertEqual('Surah Masad', scraper._surah_name)
        
    @patch('shareislam.scraper.sys')
    def test_collect_parts(self, scraper_sys):
        scraper = Scraper()
        def pass_build_parts():
            pass
        scraper.build_parts_trees = pass_build_parts
        scraper.scrape_page(self.page_html)
        self.assertEqual(0, scraper_sys.exit.call_count)

        for idx, part in enumerate(scraper._parts):
            print 'part idx:%s', idx
            pp.pprint(part) 
        
        self.assertEqual(93, len(scraper._parts))
        self.assertTrue('SECTION' in scraper._parts[0]['ptype'])
        self.assertEqual(9, len([x for x in scraper._parts if 'SECTION' in x['ptype']]))
        self.assertEqual(set(['ARABIC', 'AYAH', 'SECTION']), scraper._parts[35]['ptype'])
        
    @patch('shareislam.scraper.sys')
    def test_build_parts(self, scraper_sys):
        scraper = Scraper()
        scraper.scrape_page(self.page_html)
        self.assertEqual(0, scraper_sys.exit.call_count)
        
#         for idx, part in enumerate(scraper._parts_trees):
#             print 'tree part idx:%s', idx
#             pp.pprint(part) 
            
        scraper.build_parts_trees()
            
    @patch('shareislam.scraper.sys')
    def test_detect_part_type(self, scraper_sys):
        scraper = Scraper()
        scraper.scrape_page(self.page_html)
        self.assertEqual(0, scraper_sys.exit.call_count)
          
        parts = scraper._content_td.xpath('.//p')
          
        for idx, p in enumerate(parts):
            print 'parts idx:%s, type:%s' % (idx, scraper._detect_part_type(p))
         
        s, _ = scraper._detect_part_type(parts[47])
        self.assertEqual(set(['SECTION']), s)
          
        s, _ = scraper._detect_part_type(parts[17])
        self.assertEqual(set(['ARABIC']), s)
          
        self.assertFalse(reduce(lambda acc, p: acc or 'BULLET' in scraper._detect_part_type(p), parts, False))
          
        s, _ = scraper._detect_part_type(parts[35])
        self.assertEqual(set(['AYAH', 'ARABIC', 'SECTION']), s)
          
        s, _ = scraper._detect_part_type(parts[61])
        self.assertEqual(set([]), s)
          
        s, _ = scraper._detect_part_type(parts[8])
        self.assertEqual(set(['ARABIC', 'HADITH']), s)
          
        s, _ = scraper._detect_part_type(parts[77])
        self.assertEqual(set(['XAYAH', 'ARABIC']), s)
        
    @patch('shareislam.scraper.sys')
    def test_get_direct_surah_translation(self, scraper_sys):
        scraper = Scraper()
        scraper.scrape_page(self.page_html)
        self.assertEqual(0, scraper_sys.exit.call_count)
        
        scraper.build_parts_trees()
        self.assertEqual(5, scraper._ayah_count)
        self.assertEqual(scraper._ayah_count, len(scraper._translation.keys()))
          
    @patch('shareislam.scraper.sys')
    def test_write(self, scraper_sys):
        scraper = Scraper()
        scraper.scrape_page(self.page_html)
        self.assertEqual(0, scraper_sys.exit.call_count)
        
        with open('111.html', 'w') as html_stream:
            scraper.write_html(html_stream)
        with open('111.json', 'w') as json_stream:
            scraper.write_json(json_stream)
            
        with open('111.json', 'r') as json_stream:
            scraper = Scraper()
            scraper.read_json(json_stream)
            self.assertTrue(len(scraper._parts_trees) > 0)
            self.assertTrue(len(scraper._translation) > 0)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()