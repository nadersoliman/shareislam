# -*- coding: utf-8 -*-
'''
Created on Jun 29, 2013

@author: nader
'''
import unittest
import pprint
from shareislam.scraper import Scraper


pp = pprint.PrettyPrinter(indent=4) 
class ScraperTests(unittest.TestCase):


    def setUp(self):
        self.page_html = None
        with open('tests/fatha_tests.htm', 'r') as f:
            self.page_html = f.read()

    def tearDown(self):
        pass

#    def test_fetch_page(self):
#        scraper = Scraper()
#        page = scraper.fetch_page('http://www.quran4u.com/Tafsir%20Ibn%20Kathir/001%20Fatihah.htm')
#        self.assertTrue(page)
        
    def test_scrape_page(self):
        scraper = Scraper()
        scraper.scrape_page(self.page_html)
        self.assertEqual('Surah Al-Fatihah', scraper._surah_name)

    def test_collect_parts(self):
        scraper = Scraper()
        def pass_build_parts():
            pass
        scraper.build_parts_trees = pass_build_parts
        scraper.scrape_page(self.page_html)
        self.assertEqual(1041, len(scraper._parts))
        self.assertTrue('SECTION' in scraper._parts[0]['ptype'])
#         for idx, p in enumerate(scraper._parts_trees):
#             if p.get('ayah_idx', -1) > 0:
#                 print 'parts idx:%s' % (idx)
#                 pp.pprint(pp.pprint(p))
        
       
        #pp.pprint(scraper._parts_trees[20])
    def test_build_parts_trees(self):
        scraper = Scraper()
        scraper.scrape_page(self.page_html)
        self.assertEqual(59, len(scraper._parts_trees))
        self.assertEqual(0, len(scraper._parts))
        self.assertEqual(1, scraper._parts_trees[20]['ayah_idx'])
        self.assertEqual(2, scraper._parts_trees[27]['ayah_idx'])
        self.assertEqual(3, scraper._parts_trees[36]['ayah_idx'])
        self.assertEqual(4, scraper._parts_trees[38]['ayah_idx'])
        self.assertEqual(5, scraper._parts_trees[43]['ayah_idx'])
        self.assertEqual(6, scraper._parts_trees[51]['ayah_idx'])
        self.assertEqual(7, scraper._parts_trees[54]['ayah_idx'])
    
    def test_detect_part_type(self):
        scraper = Scraper()
        scraper.scrape_page(self.page_html)
        
        parts = scraper._tree.xpath('//p')
        
        for idx, p in enumerate(parts):
            print 'parts idx:%s, type:%s' % (idx, scraper._detect_part_type(p))
        
        s, _ = scraper._detect_part_type(parts[1051])
        self.assertEqual(set(['SECTION']), s)
        
        s, _ = scraper._detect_part_type(parts[1053])
        self.assertEqual(set(['ARABIC']), s)
        
        s, _ = scraper._detect_part_type(parts[1058])
        self.assertEqual(set(['XAYAH', 'ARABIC']), s)
        
        s, _ = scraper._detect_part_type(parts[1106])
        self.assertEqual(set(['BULLET']), s)
        
        s, _ = scraper._detect_part_type(parts[677])
        self.assertEqual(set(['AYAH', 'ARABIC', 'SECTION']), s)
        
        s, _ = scraper._detect_part_type(parts[680])
        self.assertEqual(set(['ARABIC', 'HADITH']), s)
        
        s, _ = scraper._detect_part_type(parts[654])
        self.assertEqual(set([]), s)
        
    def test_text_clearspaces(self):
        scraper = Scraper()
        #self.assertEqual(scraper._text_clearspaces(u'The Meaning of \r\nAl-Fatihah \r\nand its Various Names'),
        #                 u'The Meaning of Al-Fatihah and its Various Names')
        self.assertEqual(u'- Al-Fatihah', scraper._text_clearspaces(u'-        Al-Fatihah'))
        
#    def test_write_output(self):
#        scraper = Scraper()
#        scraper.scrape_page(self.page_html)
#        scraper.write_output('001', False)
        
    def test_arabic_to_english_numerals(self):
        scraper = Scraper()
        self.assertEqual(1, scraper._arabic_to_english_numerals('&#1633;'))
        self.assertEqual(81, scraper._arabic_to_english_numerals('&#1640;&#1633;'))
        self.assertEqual(7, scraper._arabic_to_english_numerals(u'\u0667'))
        self.assertEqual(17, scraper._arabic_to_english_numerals(u'\u0661\u0667'))
        self.assertEqual(81, scraper._arabic_to_english_numerals(u'\u0668\u0661'))
        self.assertEqual(4, scraper._arabic_to_english_numerals('\n 4')) 
        
    def test_is_arabic_text(self):
        scraper = Scraper()
        self.assertTrue(scraper._is_arabic(u'السلام عليكم ورحمة الله وبركاته'))
        self.assertFalse(scraper._is_arabic(u'hello everyone'))
        self.assertTrue(scraper._is_arabic(u'hello everyone وعليكم ورحمة الله'))
            

#    def est_scrape_index(self):
#        from shareislam.scraper import scrape_index
#        scrape_index('http://www.quran4u.com/Tafsir%20Ibn%20Kathir/Index.htm')

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
