# -*- coding: utf-8 -*-
'''
Created on Jun 29, 2013

@author: nader
'''
import sys
from StringIO import StringIO
import re
import requests
import json

import lxml.html
import lxml.etree
from lxml.html import parse, builder as H
from lxml.builder import E as X
from boto.s3.connection import S3Connection
from boto.s3.key import Key
from hashlib import md5

import log_configs
import base64
from logging import root

HEADERS = {
           'User-Agent': 'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; Media Center PC 4.0; SLCC1; .NET CLR 3.0.04320)',
           'Accept': 'text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5',
           'Accept-Language': 'ar-eg,en;q=0.7,en-us;q=0.3',
           'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.7',
           'Connection':'keep-alive'}
EXTERNAL_FETCH_TIMEOUT = 20 # in seconds 
EXTERNAL_FETCH_RETRY_COUNT = 5
REGEX_CLEARSPACES = re.compile(r'\s+', re.I | re.U)
ROOT_XPATH = ['./tr[2]/td', './tr/td[2]', './tbody/tr[2]/td', './tbody/tr/td[2]']
SURAH_NAME_XPATH = ["//font[@color='#999999'][@size='5']", "//font[@color='#999999'][@size='6']"]

logger = log_configs.configure_logger('scraper')

class Scraper(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self._parts = []
        self._parts_trees = []
        self._tree = None
        self._content_td = None
        self._sub_tables = []
        self._ayah_count = 0
        self._surah_name = ''
        self._translation = None
        self._content_table = None

    def fetch_page(self, url):
        logger.info("Fetching page @ url %s", url)
        response = requests.get(url, headers=HEADERS)
        return response.text
    
    def scrape_page(self, page):
        logger.info('Scraping page')
        self._tree = parse(StringIO(page))
        self._content_table = self._find_parent_table()
        if self._content_table is None:
            logger.error('Failed getting the parent table ')
            sys.exit(1)

        self._surah_name = self._get_surah_name()
        logger.info('Current Surah Name:%s', self._surah_name)
        xpath, _ = self._find_content_td(self._content_table)
        logger.info('Content root @ xpath:%s', xpath)

        self._content_td = self._content_table.find(xpath)
        if self._content_td is None:
            logger.error('Couldnt find root TD')
            sys.exit(1)
            
        logger.debug('Found the table parent @ %s', self._content_td)
        self.cleanup_tables()
        self.collect_parts()
        self.build_parts_trees()
        self._translation = self.get_direct_surah_translation()
        
    def _get_surah_name(self):
        logger.info('Searching for surah name')
        def _get_none_font_parent(e):
            if e.tag.lower() != 'font':
                return e
            else:
                return _get_none_font_parent(e.getparent())
            
        candidate = self._content_table.find('.//tr/td/p[2]').text_content().strip()
        if 'surah' in candidate.lower() and len(candidate) > 5 and len(candidate) < 25:
            return candidate
            
        found_tags = []
        for xpath in SURAH_NAME_XPATH:
            logger.debug('collecting candidate surah names using @ %s', xpath)
            candiates = self._tree.findall(xpath)
            if len(candiates) <= 0:
                logger.debug('neglecting xpath:%s', xpath)
            else:
                found_tags.extend(candiates)
                #parents = [_get_none_font_parent(x) for x in candiates]
                #for parent in parents:
                #    if not parent in found_tags:
                #        found_tags.append(parent)
                
        for tag in [x for x in found_tags]:
            text = tag.text_content()
            if len(text) <= 5 or self._is_arabic(text):
                logger.debug('neglecting tag with content:%s', text)
                found_tags.remove(tag)
        if len(found_tags) == 0:
            msg = 'after cleaning surah name tags, we endup with nothing'
            logger.error(msg)
            raise Exception(msg)
        if len(found_tags) > 1:
            logger.warn('we have more than one tag for surah name as follow:%s, picking the one contating surah if any', 
                        [x.text_content() for x in found_tags])
            tags = filter(lambda x: 'surah' in x.text_content().lower(), found_tags)
            found_tags = tags if len(tags) > 0 else found_tags
        return self._text_clearspaces(found_tags[0].text_content().strip())
        
    def _detect_part_type(self, part):
        ptype = set()
        text = self._text_clearspaces(part.text_content().strip())
        
        if part.attrib.get('dir', '').lower() == 'rtl':
            ptype.add('ARABIC')
        if part.xpath('.//*[contains(translate(@dir, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),"rtl")]'):
            ptype.add('ARABIC')
            
        ayah_anchor = part.find('.//a[@name]')
        if not ayah_anchor is None: #or self._has_color(part, '990000'):
            ptype.add('AYAH')
            ptype.add('SECTION')
             
        if part.attrib.get('class', '').lower() == 'msonormal':
            ptype.add('SECTION')
            
        if text.startswith('- '):
            text = text[2:]
            ptype.add('BULLET')
            
        if 'ARABIC' in ptype:
            if self._has_color(part, 'AA0000'): # in al-fatiha
                ptype.add('XAYAH')
                
            if self._has_color(part, '005978'): # in al-ikhlas
                ptype.add('XAYAH') 
                
            if self._has_color(part, '005078'):  # in Masad
                ptype.add('XAYAH')
                
            if self._has_color(part, '0066FF'): 
                ptype.add('HADITH')
                
            if self._has_color(part, '3366FF'): # in Masad
                ptype.add('HADITH')
                
            if self._has_color(part, '0066FF'):
                ptype.add('HADITH')
                
        # handling the case where having $nbsp; as a section separator
        if len(ptype) == 0 and text == '':
            ptype.add('SECTION')
        return ptype, text
    
    def _has_color(self, part, color):
        return len(part.xpath('.//*[contains(@style, "%s")]' % (color))) > 0 or len(part.xpath('.//font[contains(@color, "%s")]' % (color))) > 0
    
    def cleanup_tables(self):
        tables = self._content_td.xpath('.//table')
        logger.info('Cleaning up inside tables, %s tables', len(tables))
        for table in tables:
            logger.debug('removing table with content %s', self._text_clearspaces(table.text_content().strip()))
            self._sub_tables.append(table)
            table.getparent().remove(table)
    
    def get_direct_surah_translation(self):
        def rows_of_translation_table(stable):
            rows = stable.findall('./tr')
            number_of_columns = len(rows[0].findall('./td'))
            logger.debug("finding translation table, found  table with rows:%s, columns:%s", len(rows), number_of_columns)
            if len(rows)-1 == self._ayah_count and number_of_columns == 2:
                return rows
            else:
                None
        
        def rows_of_translation_table_combined(stables):
            tables_2_columns = []
            for stable in stables:
                row = stable.find('./tr')
                if len(row.findall('./td')) == 2:
                    tables_2_columns.append(stable)
            rows = []
            for stable in tables_2_columns:
                rows.extend(stable.findall('./tr'))
            if len(rows)-1 == self._ayah_count:
                return rows
            else:
                None
        
        logger.info('Finding direct ayah translation table')
        if len(self._sub_tables) > 1:
            logger.warning('found more than one sub table, trying to figure out which has the translation')
        elif len(self._sub_tables) == 0:
            logger.error('Could not find a translation table')
            return

        rows = None      
        for stable in self._sub_tables:
            rows = rows_of_translation_table(stable)
            if not rows is None:
                break 
            
        if rows is None:
            logger.warning('Although found %s sub-tables they are not good candidates based on # of rows and # of columns to be considered translation tables now trying to combine',
                               len(self._sub_tables))
            rows = rows_of_translation_table_combined(self._sub_tables)
            if rows is None:
                logger.warning('Even after combining subtables we didnt find proper translation with ayah count:%s', self._ayah_count)
                return None
        
        translation = {}
        for idx, row in enumerate(rows[1:]):
            translation_column = row.findall('./td')[1] 
            two_ps = [x for x in translation_column.findall('./p') if len(self._text_clearspaces(x.text_content())) > 0]
            if len(two_ps) != 2:
                logger.error("In translation table found a row P tags length not 2, len(two_ps):%s", len(two_ps))
            translation[idx+1] = {'arabic':self._text_clearspaces(two_ps[0].text_content()),
                                 'english':self._text_clearspaces(two_ps[1].text_content())}
        return translation
        
    def collect_parts(self):
        logger.info('Collection of Parts')
        if len(self._parts) > 0 or len(self._parts_trees) > 0:
            logger.warning('I have collected parts once before! len(_parts):%s, len(_parts_trees):%s', 
                                 len(self._parts), len(self._parts_trees))
            self._reinitialize()
            
        parts = self._content_td.findall('./p')
        for part in parts:
            ptype, text = self._detect_part_type(part)
            self._parts.append({'part':part, 'ptype':ptype, 'sub':[], 'text':[text]})
            
        if len(''.join(self._parts[len(self._parts)-1]['text']).replace('*', '')) == 0:
            logger.debug("Removing end section containing ******")
            self._parts.pop()
            
        if len(self._parts) < 20:
            logger.error('tried %s and all parts coming less than 20 parts, aborting', ROOT_XPATH)
            sys.exit(1)
        else:
            logger.info("Collect %s parts", len(self._parts))
            
    def _find_content_td(self, table):
        logger.info('Searching for table row @ %s', ROOT_XPATH)
        counts = {}
        for xpath in ROOT_XPATH:
            logger.debug('collecting Parts using content root @ %s', xpath)
            table_row = table.find(xpath)
            if table_row is None:
                logger.debug('didnt find table row @ %s', xpath)
                continue
            else:
                counts[xpath] = len(table_row.findall('./p'))
        if len(counts) == 0:
            logger.error('Didnt find the content td in xpath:%s', ROOT_XPATH)
            sys.exit(1)
        logger.debug('collected paragraphs for different xpath is %s', counts)
        xpath = max(counts.keys(), key=counts.get)
        if counts[xpath] < 20:
            logger.warning('based on paragraphs collected counts:%s, xpath:%s is the best with %s less than 20', counts, xpath, counts[xpath])
        else:
            logger.debug('based on paragraphs collected counts:%s, xpath:%s is the best with %s less than 20', counts, xpath, counts[xpath])
        return xpath, counts[xpath]
    
    def build_parts_trees(self):
        logger.info('Building parts Parts')
        idx = 0
        while idx < len(self._parts):
            if 'SECTION' in self._parts[idx]['ptype']:
                idx = self._process_section(idx)
            else:
                idx += 1
            
    def _process_section(self, idx):
        part = self._parts[idx]
        self._parts_trees.append(part)
        self._parts.remove(part)

        if 'AYAH' in part['ptype']:
            self._ayah_count += 1
            ayah_anchor = part['part'].find('.//a[@name]')
            if not ayah_anchor is None:
                part['ayah_idx'] = self._get_ayah_index_from_anchor(ayah_anchor)
            else:
                part['ayah_idx'] = None
            part['ayah_text'] = part['text'] 
        
        while idx < len(self._parts):
            next_part = self._parts[idx]
            section_end_flag = 'SECTION' in next_part['ptype'] or (len(next_part['ptype']) == 0 and sum([len(x) for x in next_part['text']], 0) == 0)  
            if not section_end_flag:
                self._process_sub_section(part, idx)
            else:
                break 
        return idx
    
    def _get_ayah_index_from_anchor(self, anchor):
        anchor_name = anchor.attrib['name']
        
        try:
            return self._arabic_to_english_numerals(anchor_name)
        except:
            try:
                # happens sometimes the anchor is not numeric, so we lookup the main table index
                return self._arabic_to_english_numerals(self._content_table.xpath('//a[@href="#%s"]' % (anchor_name))[0].text_content())
            except:
                pass
    
    def _process_sub_section(self, parent_part, idx):
        part = self._parts[idx]
        parent_part['sub'].append(part)
        self._parts.remove(part)
        while idx < len(self._parts):
            next_part = self._parts[idx]
            if not 'SECTION' in next_part['ptype']:
                if 'HADITH' in next_part['ptype']:
                    self._process_hadith(part, idx)
                elif 'BULLET' in next_part['ptype']:
                    self._process_bullet(part, idx)
                elif 'XAYAH' in next_part['ptype']:
                    self._process_xayah(part, idx)
                else:
                    break
                    #self._parts.remove(next_part)
                    #part['sub'].append(next_part)
            else:
                break 
    
    def _process_hadith(self, parent_part, idx):
        part = self._parts[idx]
        parent_part['ptype'] = parent_part['ptype'].union(part['ptype'])
        parent_part['sub'].append(part)
        self._parts.remove(part)
        while idx < len(self._parts):
            next_part = self._parts[idx]
            if 'HADITH' in next_part['ptype']:
                part['text'].extend(next_part['text'])
                #part['sub'].append(next_part) we don't care anymore of the parts of the haddith we collected it in this part text
                self._parts.remove(next_part)
            else:
                break

    def _process_bullet(self, parent_part, idx):
        part = self._parts[idx]
        parent_part['ptype'] = parent_part['ptype'].union(part['ptype'])
        parent_part['sub'].append(part)
        self._parts.remove(part)
        while idx < len(self._parts):
            next_part = self._parts[idx]
            if 'BULLET' in next_part['ptype']:
                part['text'].extend(next_part['text'])
                #part['sub'].append(next_part) we don't care anymore of the parts of the bullet we collected it in this part text
                self._parts.remove(next_part)
            else:
                break

    def _process_xayah(self, parent_part, idx):
        part = self._parts[idx]
        parent_part['ptype'] = parent_part['ptype'].union(part['ptype'])
        parent_part['sub'].append(part)
        self._parts.remove(part)
        while idx < len(self._parts):
            next_part = self._parts[idx]
            if 'XAYAH' in next_part['ptype']:
                part['text'].extend(next_part['text'])
                #part['sub'].append(next_part) we don't care anymore of the parts of the xayah we collected it in this part text
                self._parts.remove(next_part)
            else:
                break
            
    def _dump_html(self, stream):
        logger.info('building html')
        html = H.HTML(
                      H.HEAD(
                             H.LINK(rel="stylesheet", href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css", type="text/css"),
                             H.LINK(rel="stylesheet", href="main.css", type="text/css"),
                             H.TITLE(self._surah_name)
                             ),
                      H.BODY(
                             H.DIV(H.CLASS("container"),
                                   H.DIV(H.CLASS("row text-center"), H.H1(self._surah_name, {'data-surah-name': self._surah_name})),
                                   H.DIV(H.CLASS("row"),
                                         H.DIV(H.CLASS("span4"),
                                               H.DIV(H.CLASS("row"), 
                                                     H.DIV(H.CLASS("span2 legend")),
                                                     H.DIV(H.CLASS("span2"))),
                                               H.DIV(H.CLASS("row"), 
                                                     H.DIV(H.CLASS("span2"), H.H4("Table of Contents")),
                                                     H.DIV(H.CLASS("span2 ayah_drop"))),
                                               H.DIV(H.CLASS("row"),
                                                     H.DIV(H.CLASS("span4 toc")))),
                                         H.DIV(H.CLASS("span8"), lxml.etree.Comment(text='Content')))
                                   )
                             )
                      )
        
        toc = html.find('.//div[@class="span4 toc"]')
        #toc.append(H.H4("Table of Contents"))
        side_nav = H.UL(H.CLASS('nav nav-tabs nav-stacked'), id='sidenav')
        toc.append(side_nav)
        
        cont_html = html.find('.//div[@class="span8"]')
        
        sections_html = H.DIV(H.CLASS('sections'))
        cont_html.append(sections_html)
        
        idx = 0
        subject_counter = 1
        ayah_list = []
        for part in self._parts_trees:
            logger.debug('writing out parts_trees idx:%s', idx)
            root_xml = None
            if 'AYAH' in part['ptype']:
                ayah_idx = part['ayah_idx']
                if ayah_idx is None:
                    ayah_idx = ''
                anchor = u'ayah_%s' % (ayah_idx)
                if not ayah_idx == '':
                    ayah_list.append(ayah_idx)
                ayah_xml = H.DIV(H.CLASS('ayah'), {'data-ayah-index':unicode(ayah_idx)}, H.A(name=anchor))
                ayah_xml.append(H.H2(H.CLASS('arabic text-error'), ' '.join(part['ayah_text'])))
                tafsir_xml = H.DIV(H.CLASS('tafisr'))
                ayah_xml.append(tafsir_xml)
                sections_html.append(ayah_xml)
                root_xml = tafsir_xml
            else:
                anchor = u'toc_%s' % (subject_counter)
                title = u'\r'.join(part['text'])
                if len(title) == 0:
                    nex = part['part'].getnext()
                    prev = part['part'].getprevious()
                    nex = nex.text_content() if not nex is None else ''
                    prev = prev.text_content() if not prev is None else ''
                    logger.warning('Empty Subject @part:%s, @next:%s, @previous:%s', part, nex, prev)
                    continue
                side_nav.append(H.LI(H.A(title, href='#' + anchor)))
                subject_html = H.DIV(H.CLASS('subject'), H.A(name=anchor))
                subject_html.append(H.H3(title))
                if self._is_arabic(title):
                    subject_html.attrib['class'] = ' '.join([subject_html.attrib.get('class', ''), 'arabic'])
                sections_html.append(subject_html)
                root_xml = subject_html
                subject_counter += 1
                    
            for sub in part['sub']:
                self._write_html_sub_part(root_xml, sub)
            idx += 1
                
        ayah_ref = html.xpath('.//div[@class="span2 ayah_drop"]')[0]
        select_box = H.SELECT(H.CLASS('input-small'), id="ayah_idx")
        for idx in ayah_list:
            select_box.append(H.OPTION(unicode(idx), value='ayah_%s' % idx))
        ayah_ref.append(H.DIV(H.CLASS('input-prepend'),
                        H.SPAN(H.CLASS('add-on'), 'Ayah Index'),
                                 select_box));
        
        legend = html.xpath('.//div[@class="span2 legend"]')[0]
        legend.append(lxml.html.fromstring('''
            <ul class="unstyled">
                <h4 style="margin-top:0px;">Legend</h4>
                <li><span class="label label-important">Ayah</span></li>
                <li><span class="label label-info">Hadith</span></li>
                <li><span class="label label-success">External Ayah</span></li>
            </ul>
        '''))
        
        # Translations
        title = '%s Translation' % (self._surah_name)
        side_nav.append(H.LI(H.A(title, href='#translation')))
        translation_html = H.DIV(H.CLASS('translation subject'), H.A(name='translation'), H.H3(title))
        for _, val in self._translation.iteritems():
            row = H.DIV(H.CLASS('row'))
            row.append(H.DIV(H.CLASS('span4'), H.P(H.CLASS('ayah-translation'), val['english'])))
            row.append(H.DIV(H.CLASS('span4'), H.P(H.CLASS('ayah arabic text-error'), val['arabic'])))
            translation_html.append(row)
        sections_html.append(translation_html)
        
        self._add_js(html)
                
        stream.write(lxml.html.tostring(html, pretty_print=True))
                
                
    def _write_html_sub_part(self, xml_root, part):
        sub_xml = None
        if 'HADITH' in part['ptype']:
            text = '\r'.join(part['text'])
            hadith_html = H.P(text)
            if self._is_arabic(text):
                hadith_html.attrib['class'] = ' '.join([hadith_html.attrib.get('class', ''), 'arabic text-info'])
            sub_xml = H.DIV(H.CLASS('hadith'))
            sub_xml.append(hadith_html)
            for b in part['sub']:
                self._write_html_sub_part(sub_xml, b)
        elif 'BULLET' in part['ptype']:
            if len(part['sub']) > 1:
                pass
            assert len(part['sub']) <= 1
            text = '\r'.join(part['text'])
            sub_xml = H.P(text)
            if self._is_arabic(text):
                sub_xml.attrib['class'] = ' '.join([sub_xml.attrib.get('class', ''), 'arabic'])
            ul_xml = H.UL()
            xml_root.append(ul_xml)
            if len(part['sub']) > 0:
                for t in part['sub'][0]['text']:
                    ul_xml.append(H.LI(t))
        elif 'XAYAH' in part['ptype']:
            text = '\r'.join(part['text'])
            ayah_html = H.P(text)
            if self._is_arabic(text):
                ayah_html.attrib['class'] = ' '.join([ayah_html.attrib.get('class', ''), 'arabic text-success'])
            sub_xml = H.DIV(H.CLASS('external-ayah'))
            sub_xml.append(ayah_html)
            for b in part['sub']:
                self._write_html_sub_part(sub_xml, b)
        else:
            text = '\r'.join(part['text'])
            sub_xml = H.P(text)
            if self._is_arabic(text):
                sub_xml.attrib['class'] = ' '.join([sub_xml.attrib.get('class', ''), 'arabic'])
            div_xml = H.DIV()
            for b in part['sub']:
                self._write_html_sub_part(div_xml, b)

        xml_root.append(sub_xml)

    def write_json(self, outstream):
        def remove_part(part):
            part.pop('part')
            for sub in part['sub']:
                remove_part(sub)
            return part
         
        parts_trees = []
        for part in self._parts_trees:
            parts_trees.append(remove_part(part))
            
        json.dump({'translation':self._translation, 'parts_trees':parts_trees}, 
                  outstream, indent=2, cls=PythonObjectEncoder, ensure_ascii=False)
        
    def read_json(self, instream):
        self._reinitialize()
        obj = loads(instream.read(), object_hook=as_python_object)
        self._parts_trees = obj['parts_trees']
        self._translation = obj['translation']
            
    def write_html(self, outstream):
        self._dump_html(outstream)
    
    def upload_html(self, surah_number, instream):
        instream.seek(0)
        f_name = '.'.join([surah_number, 'html'])
            
        logger.info('uploading %s to s3', f_name)
        conn = S3Connection()
        bucket = conn.get_bucket('static.kotobkhana.com')
        
        key = Key(bucket, '/'.join(['shareislam', f_name]))
        key.content_type = 'text/html'
        key.content_encoding = 'utf8'
        key.set_contents_from_file(instream)
        
        logger.info('uploading main.css to s3')
        css_key = Key(bucket, 'shareislam/main.css')
        css_key.content_type = 'text/css'
        css_key.content_encoding = 'utf8'
        css_key.set_contents_from_filename('main.css')
        
    def hexmd5(self, instream):
        instream.seek(0)
        m = md5()
        m.update(instream.read())
        instream.seek(0)
        return m.hexdigest(), base64.encodestring(m.digest())
    
    def upload_json(self, surah_number, instream):
        instream.seek(0)
        f_name = '.'.join([surah_number, 'json'])
            
        logger.info('uploading %s to s3', f_name)
        conn = S3Connection()
        bucket = conn.get_bucket('static.kotobkhana.com')
        
        key = Key(bucket, '/'.join(['shareislam', f_name]))
        key.content_type = 'application/octet-stream'
        key.content_encoding = 'utf8'
        #key.set_contents_from_file(instream)
        # I don't know what's up with the .json so writing it on disk then uploading
        with open(f_name, 'w') as f_json:
            f_json.write(instream.read())
        key.set_contents_from_filename(f_name)
                
    def _reinitialize(self):
        self._parts = []
        self._parts_trees = []
        self._ayah_count = 0
        self._sub_tables = []
        self._translation = None
        self._content_table = None
                
    def _add_js(self, html):
        body = html.find('body')
        body.extend([H.SCRIPT(src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"),
                     H.SCRIPT(src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"),])
        body.append(H.SCRIPT(
'''
$(function(){
    //$('#sidenav').affix()
    $("#ayah_idx").change(function(ev){
        console.log($("#ayah_idx").val());
        window.location.href = window.location.href.split('#')[0] + '#' + encodeURIComponent($("#ayah_idx").val());  
    });
});
'''))
        
    def _is_arabic(self, t):
        if len(t) == 0:
            return False
        ar_count = reduce(lambda acc, c: acc+1 if ord(c) > 256 else acc, t, 0)
        return (float(ar_count)/float(len(t))) > 0.4
    
    def _find_parent_table(self):
        '''
        finds the parent table starting at the child page navigation link
        '''
        def _recursive(e):
            if e is None:
                return None
            
            if e.tag.lower() == 'table':
                return e
            else:
                return _recursive(e.getparent())
        
        e = self._tree.find('//table//a[@name="Page Navigation"]')
        if not e is None:
            logger.debug('Found the Page Navigation link, now looking for parent table')
        else:
            logger.error('Failed in find the page navigation link')
            return None
        
        return _recursive(e)
        
    def _text_clearspaces(self, t):
        if t.startswith('-'):
            pass
        clean = REGEX_CLEARSPACES.sub(u' ', t)
        clean = clean.replace(u'&nbsp;', u' ')
        clean = clean.replace(u'&#160;', u' ')
        if clean == t:
            empty = len(clean.replace(u' ', u'')) == 0
            return clean if not empty else ''
        else:
            return self._text_clearspaces(clean)
    
    def _arabic_to_english_numerals(self, entity):
        entity = unicode(self._text_clearspaces(entity)).strip()
        try:
            return int(entity)
        except:
            pass
        numbers = []
        if not '&#' in entity:
            for c in entity:
                numbers.append(ord(c))
        else:
            for e in entity.split(';'):
                if not e:
                    continue
                c = e.replace('&#', '')
                numbers.append(int(c))
        return int(u''.join([unicode(x - 1632) for x in numbers]))
        
from json import dumps, loads, JSONEncoder, JSONDecoder
import pickle

class PythonObjectEncoder(JSONEncoder):
    '''
    http://stackoverflow.com/questions/8230315/python-sets-are-not-json-serializable
    '''
    def default(self, obj):
        if isinstance(obj, (list, dict, int, unicode, str, float, bool, type(None))):
            return JSONEncoder.default(self, obj)
        elif isinstance(obj, set):
            return list(obj)
        return {'_python_object': pickle.dumps(obj)}

def as_python_object(dct):
    if '_python_object' in dct:
        return pickle.loads(str(dct['_python_object']))
    return dct        

def scrape_index(url):
    logger.info('Scraping index.html file')
    surah_list = []
    tree = None
    with open('index.html', 'r') as index_html:
        tree = parse(StringIO(index_html.read()))
    hrefs = tree.xpath('//a[text()="Download"]')
    for href in hrefs:
        remote_f = href.attrib['href']
        parts = remote_f.split('%20')
        scraped_f = parts[0]
        if len(parts) == 3:
            scraped_f = '_'.join([parts[0], parts[2].split('.')[0]])
        if parts[0] == '113-114':
            parts[0] = '113'
        surah_list.append({'index':int(parts[0]), 
                           'url':'http://www.quran4u.com/Tafsir%%20Ibn%%20Kathir/%s' % (remote_f),
                           'file':scraped_f})
    with open('index.json', 'w') as f:
        json.dump(surah_list, f, indent=2, cls=PythonObjectEncoder, ensure_ascii=False)
    