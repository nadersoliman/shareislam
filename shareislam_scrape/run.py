# -*- coding: utf-8 -*-
'''
Created on Jun 30, 2013

@author: nader
'''
import sys, re
from StringIO import StringIO
import json

from shareislam.scraper import Scraper
 
def scrape(surah):
    scraper = Scraper()
    scraper.scrape_page(scraper.fetch_page(surah['url']))
    
    json = StringIO()
    html = StringIO()
    
    scraper.write_html(html)
    scraper.write_json(json)
    
    scraper.upload_html(surah['file'], html)
    scraper.upload_json(surah['file'], json)
    
if __name__ == '__main__':
    surah_to_scrape = None
    with open('index.json', 'r') as index_f:
        surah_to_scrape = json.load(index_f)
        
    if len(sys.argv) != 2:
        print 'Scraping All Surahs'
    else:
        surah_num = int(sys.argv[1])
        print 'Scraping Surah #%s' % (surah_num)
        surah_to_scrape = [s for s in surah_to_scrape if s['index'] == surah_num]
        
    for surah in surah_to_scrape:
        scrape(surah)
